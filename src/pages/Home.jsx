import { Banner } from "../components/Navbar/Banner"
import { Buscador } from "../components/Buscador/Buscador"
import { ListadoPersonajes } from "../components/Personajes/ListadoPersonajes"

export function Home() {
    return <div>
        <Banner />
        <div className="container">
            <h1 className="py-4 text-info">Personajes destacados</h1>
            <Buscador />
            <ListadoPersonajes />
        </div>
    </div>
}