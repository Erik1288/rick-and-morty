import { Link } from "react-router-dom"

export function Footer() {
    return (
        <div className="navbar-dark bg-info">
            <p className="text-center text-white"> Proyecto L.s.v-tech, Car-Iv 2022</p>
            <div>
                <a href="https://lsv-tech.com/"><img src="src/assets/images/lsv6.jpeg" alt="lsv" /></a>
            </div>
        </div >
    )
}