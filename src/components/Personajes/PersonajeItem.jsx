const estiloCirculo = {
    width: '10px',
    height: '10px',
    backgroundColor: 'green',
    display: "inline-block",
    borderRadius: "50%",
    marginRight: "10px"
}

export function PersonajeItem({ name, status, species, location, image, origin }) {
    return (
        <div className="col-4">
            <div className="card mb-3" style={{ maxWidth: '540px' }}>
                <div className="row g-0">
                    <div className="col-md-4">
                        <img src={image} className="img-fluid rounded-start" alt={name} />
                    </div>
                    <div className="col-md-8">
                        <div className="card-body">

                            <h5 className="card-title mb-0">{name}</h5>

                            <p><span style={estiloCirculo}></span>{status} - {species}</p>

                            <p className="mb-0 text-muted">Last know location</p>
                            <p>{location?.name}</p>

                            <p className="mb-0 text-muted">Origin:</p>
                            <p>{origin?.name}</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}