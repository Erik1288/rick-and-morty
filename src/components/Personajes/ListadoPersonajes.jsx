import { PersonajeItem } from "./PersonajeItem"
import { useState, useEffect } from "react"
import axios from "axios"

export function ListadoPersonajes() {
    const [personajes, setPersonajes] = useState(null);

    useEffect(() => {
        axios.get('https://rickandmortyapi.com/api/character/').then((respuesta) => {
            setPersonajes(respuesta.data)
        })
    }, [])

    return (
        <div className="row py-5">
            {personajes
                ? personajes.results.map((personaje) => {
                    return <PersonajeItem key={personaje.id} {...personaje} />
                }) : 'Carganndo...'}
        </div>
    )
}